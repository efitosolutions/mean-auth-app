import { Component, OnInit } from '@angular/core';
import {ValidateService} from '../../services/validate.service'
import {FlashMessagesModule, FlashMessagesService} from 'angular2-flash-messages';
import {AuthService} from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  name: String;
  username: String;
  email: String;
  password: String;

  constructor(
    private validateService: ValidateService,
    private flashmessages:FlashMessagesService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {

  }

  onRegisterSubmit(){
    const user = {
      name: this.name,
      username: this.username,
      email: this.email,
      password: this.password
    }

    if(!this.validateService.validateRegister(user)){
      this.flashmessages.show('Please fill in the all fields', {cssClass: 'alert-danger', timeout:3000});
      return false;
    }

    if(!this.validateService.validateEmail(user.email)){
      this.flashmessages.show('Please use valide email', {cssClass: 'alert-danger', timeout:3000});
      return false;
    }

    this.authService.registerUser(user).subscribe((data:any) =>{
      if(data.success){
        this.flashmessages.show('Register success', {cssClass: 'alert-success', timeout:3000});
        this.router.navigate(['/login']);
      }else{
        this.flashmessages.show('Somthing went wrong', {cssClass: 'alert-danger', timeout:3000});
        this.router.navigate(['/register']);
      }
    })

  }

}
