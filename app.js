const express = require('express');
const path = require('path');
const bodyParser = require('body-Parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');

//connect to database
mongoose.connect(config.database);

//on connection
mongoose.connection.on('connected', () =>{
    console.log('Connected to database '+config.database);
})

//on error
mongoose.connection.on('error', (err) =>{
    console.log('Connected to database '+err);
})

const app = express();

const users = require('./routes/users');

const port = 3000;

app.use(cors());

app.use(bodyParser.json());

app.use('/users', users)

app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) =>{
    res.send('Invalid endpoint');
});

app.get('*', (req, res)=>{
    res.sendFile(path.join(__dirname, 'public/index.html'));
})

app.listen(port, () =>{
    console.log('server started' +port);
});