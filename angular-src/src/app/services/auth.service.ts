// import { Injectable } from '@angular/core';
// import { Http, Headers } from '@angular/http';
// import 'rxjs/add/operator/map';
// import { map } from 'rxjs/operators';
//
// @Injectable({
//   providedIn: 'root'
// })
// export class AuthService {
//   authTocken: any;
//   user: any;
//
//   constructor(private http) { }
//
//   registerUser(user){
//     let headers = new Headers();
//     headers.append('Content-Type','application/json');
//     return this.http.post('http://localhost:3000/user/register', user,{headers: headers})
//       .map(res => res.json());
//   }
//
// }

import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { map } from 'rxjs/operators';
import {pipe} from "rxjs/internal-compatibility";
import { HttpClient,HttpHeaders } from '@angular/common/http';
//import 'rxjs/add/operator/map';
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authTocken: any;
  user: any;

  constructor(private http:HttpClient) { }

  registerUser(user){
    let headers = new HttpHeaders()
    headers.append('Content-Type','application/json')
    return this.http.post('http://localhost:3000/users/register', user, {headers})
  }

  athenticateUser(user){
    let headers = new HttpHeaders()
    headers.append('Content-Type','application/json')
    return this.http.post('http://localhost:3000/users/authenticate', user, {headers})
  }

  // getProfile(){
  //   let headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Authorization': this.authTocken
  //   })
  //   this.loadToken();
  //   headers.append('Authorization', this.authTocken);
  //   headers.append('Content-Type','application/json')
  //   return this.http.get('http://localhost:3000/users/profile', {headers})
  // }

  getProfile(): any{
    this.loadToken();
    let headers = new HttpHeaders().append('Authorization', this.authTocken).append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/users/profile', {headers: headers})
  }

  loadToken(){
    const token = localStorage.getItem('id_token');
    this.authTocken = token;
  }

  storeUserData(token: any, user: any) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authTocken = token;
    this.user = user;
  }

  helper = new JwtHelperService();

  loggedIn() {
    this.loadToken();
    return !this.helper.isTokenExpired(this.authTocken);
  }

  logout(){
    this.authTocken = null;
    this.user = null;
    localStorage.clear();
  }
}
